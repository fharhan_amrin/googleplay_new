<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Pencarian extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelPencarian');
    }


    public function index()
    {
        $data['karyawan'] = $this->ModelPencarian->data(); //menampilkan seluruh data karyawan
        $this->load->view('view', $data);
    }

    public function cari()
    {
        $keyword = $this->input->get('cari', TRUE); //mengambil nilai dari form input cari
        $data['karyawan'] = $this->ModelPencarian->cari($keyword); //mencari data karyawan berdasarkan inputan
        $this->load->view('view', $data); //menampilkan data yang sudah dicari
    }
    public function multi()
    {
        // if ($_GET['nama'] != "") {
        //     # code...
        // }
        $nama = $this->input->get('nama', TRUE); //mengambil nilai dari form input cari
        $jeniskelamin = $this->input->get('jenkel', TRUE); //mengambil nilai dari form input cari
        $data['karyawan'] = $this->ModelPencarian->cari_multi($nama, $jeniskelamin); //mencari data karyawan berdasarkan inputan
        $this->load->view('view', $data);
    }
}

/* End of file Pencarian.php */