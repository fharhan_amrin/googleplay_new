<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data extends CI_Controller
{
    private $filename = "data";
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->model('ModelData');
    }


    public function cetak()
    {
        $nama = "2971557143835";
        // $tampung = strlen($nama);
        echo date('Y-m-d',substr($nama, 3));
        
        // echo time();
    }


    public function index()
    {
        //konfigurasi pagination
        $config['base_url'] = site_url('Data/index'); //site url
        $config['total_rows'] = $this->db->count_all('vouchergamegoogleplay'); //total row
        $config['per_page'] = 10;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
        // $data['data'] = $this->mahasiswa_model->get_mahasiswa_list($config["per_page"], $data['page']);

        $data['pagination'] = $this->pagination->create_links();
        $data['data'] = $this->ModelData->load_all_vouchergamegoogleplay($config["per_page"], $data['page']);
        $this->load->view('viewGooglePlay', $data);
    }

    public function multi()
    {
        //konfigurasi pagination
        $config['base_url'] = site_url('Data/index'); //site url
        $config['total_rows'] = $this->db->count_all('vouchergamegoogleplay'); //total row
        $config['per_page'] = 10;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;


        $data['pagination'] = $this->pagination->create_links();
        $refillId = $this->input->get('refillId', TRUE); //mengambil nilai dari form input cari
        $tanggalPurchase = $this->input->get('tanggalPurchase', TRUE); //mengambil nilai dari form input cari
        $data['data'] = $this->ModelData->cari_multi($refillId, $tanggalPurchase, $config["per_page"], $data['page']); //mencari data karyawan berdasarkan inputan
        $this->load->view('viewGooglePlay', $data);
    }

    // import data excel
    public function import()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Upload
        $this->filename = $this->upload();

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('uploads/googleplay/' . $this->filename); // Load file yang telah diupload ke folder excel
        $getSheet = $loadexcel->getSheetNames();

        foreach ($getSheet as $rows) {
            $sheet = $loadexcel->getSheetByName($rows)->toArray(null, true, true, true);
            // $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
            // var_dump($sheet);
            // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
            $data = [];

            $numrow = 1;
            foreach ($sheet as $row) {
                // Cek $numrow apakah lebih dari 1
                // Artinya karena baris pertama adalah nama-nama kolom
                // Jadi dilewat saja, tidak usah diimport
                if ($numrow > 1) {
                    // Kita push (add) array data ke variabel data

                    array_push($data, array(

                        'serialNumber' => $this->cekSn($row['A'], $row['B'])['sn'], // Serial Number
                        'tanggalUpload' => date('Y-m-d'),
                        'code' => $row['B'],
                        'refillId' => $rows,
                        'status' => 0,
                        'status_voucher' => $this->cekSn($row['A'], $row['B'])['status'],
                         // Insert data alamat dari kolom D di excel
                    ));
                }

                $numrow++; // Tambah 1 setiap kali looping
            }

            // echo json_encode($data);
            // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
            try {
                $q = $this->db->insert_batch('vouchergamegoogleplay', $data);
                // $q = $this->dm->insert_multiple($data);
                if ($q) {

                    redirect("http://localhost/pencarian/");
                } else {
                    redirect("http://localhost/pencarian?err=1");
                }
            } catch (Exception $th) {
                echo $th->getMessage();
                // redirect('http://localhost/googleplay?err=1');
            }
        }
        // Redirect ke halaman awal (ke controller siswa fungsi index)
        $log = [
            'status' => true,
            'msg' => "Berhasil Import",
        ];
        echo json_encode($log);
    }


    public function cekSn($sn = "", $voucher = "")
    {
        $log =  [];
        if ($sn == "") {
            $sub = $voucher[0] . $voucher[1] . $voucher[2];
            $data = $sub . time();
            $log=[
            'sn' => $data,
            'status' => '0'
            ];
        } else {
            $data = $sn;
            $log = [
                'sn' => $data,
                'status' => '1'
            ];

        }

        return $log;
    }

    public function cekStatusVoucher($status = "")
    {
        if ($status == "") {
            
        }

        return $data;
    }

    

    
    

    

    //upload excel
    public function upload()
    {
        $config['upload_path'] = "./uploads/googleplay";
        $config['allowed_types'] = 'xlsx|csv';
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload("file")) {
            $data = array('upload_data' => $this->upload->data());

            $image = $data['upload_data']['file_name'];
            return $image;
        }
    }
}

/* End of file Data.php */