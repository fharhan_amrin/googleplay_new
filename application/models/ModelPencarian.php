<?php


defined('BASEPATH') or exit('No direct script access allowed');

class ModelPencarian extends CI_Model
{

    public function data()
    {
        return $this->db->get('karyawan')->result();
    }

    public function cari($keyword)
    {
        $this->db->like('nama', $keyword)->or_like('jenkel', $keyword); //mencari data yang serupa dengan keyword
        return $this->db->get('karyawan')->result();
    }

    public function cari_multi($nama = "", $jeniskelamin = "")
    {
        if ($nama != "") {
            $query = "`nama` LIKE '%$nama%' ";
        } else if ($jeniskelamin != "") {
            $query = "`nama` LIKE '$nama' ";
        } else if ($nama != "" && $jeniskelamin != "") {
            $query = " `nama` LIKE '$nama' AND `jenkel` = '$jeniskelamin'";
        } else {
            $query = " `nama` LIKE '$nama' AND `jenkel` = '$jeniskelamin'";
        }

        $query = $this->db->query("SELECT * FROM `karyawan` WHERE $query ");
        return $query->result();
    }
}

/* End of file ModelPencarian.php */