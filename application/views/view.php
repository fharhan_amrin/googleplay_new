<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<head>
    <title>Cara Membuat Search Sederhana Dengan Codeigniter -CodeCoding</title>
</head>

<body>
    <p>Percobaan</p>
    <form action="<?php echo site_url('Pencarian/multi'); ?>" method="get">
        <input type="text" name="nama" placeholder="Nama Karywan"
            value="<?php echo (isset($_GET['nama'])) ? $_GET['nama'] : ''; ?>">

        <input type="text" name="jenkel" placeholder="Enter jeniskelamin"
            value="<?php echo (isset($_GET['jenkel'])) ? $_GET['jenkel'] : ''; ?>">

        <button type="submit">Cari Data</button> <a href="<?php echo site_url('Pencarian'); ?>"
            style="text-decoration:none; color: black;">Reset</a>
    </form>
    <center>
        <table border="1">
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>No Telp</th>
            </tr>
            <?php
            if (count($karyawan) > 0) {
                $no = 1;
                foreach ($karyawan as $k) { ?>
            <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $k->nama; ?></td>
                <td><?php echo $k->jenkel; ?></td>
                <td><?php echo $k->alamat; ?></td>
                <td><?php echo $k->notelp; ?></td>
            </tr>
            <?php }
        } else { ?>
            <tr>
                <td colspan="5" align="center">Tidak Ada Data.</td>
            </tr>
            <?php } ?>
        </table>
    </center>
</body>

</html>