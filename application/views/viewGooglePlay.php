<!-- light-blue - v4.0.1 - 2018-10-31 -->
<?php

function ubahStatus($status = "")
{
    if ($status != "") {
        switch ($status) {
            case '0':
                $data = 'non melon';
                break;
            case '1':
                $data = 'melon';
                break;
            default:
                $data = 'tidak di ketahui';
                break;
        }
        return $data;
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Voucher Google Play</title>

    <link href="<?php echo base_url('css/application.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/toastr.css'); ?>" rel="stylesheet">

    <!-- DataTables -->
    <link href="<?php echo base_url('plugins/datatables/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet"
        media="all" type="text/css" />
    <link href="<?php echo base_url('plugins/datatables/buttons.bootstrap4.min.css'); ?>" rel="stylesheet" media="all"
        type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="<?php echo base_url('plugins/datatables/responsive.bootstrap4.min.css'); ?>" rel="stylesheet"
        media="all" type="text/css" />
    <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
    <link rel="shortcut icon" href="img/google_play.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <script>
    /* yeah we need this empty stylesheet here. It's cool chrome & chromium fix
           chrome fix https://code.google.com/p/chromium/issues/detail?id=167083
                      https://code.google.com/p/chromium/issues/detail?id=332189
        */
    </script>
</head>

<body>

    <div class="content container ">
        <h2 id="titlePage" class="page-title">Voucher Game <small id="subtitlePage">Management</small></h2>
        <div class="konten element-animation">

        </div>

        <div class="row">
            <div class="col-lg-12">
                <section class="widget">
                    <div class="row">

                        <div class="col-lg-6">
                            <div id="container"
                                style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                        </div>
                        <div class="col-lg-6">
                            <div id="containerDetail"
                                style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">
                            </div>
                        </div>
                    </div>
                </section>
                <section class="widget">
                    <header>
                        <h4>
                            Google Play Voucher List
                            <small>

                            </small>
                        </h4>
                        <div class="widget-controls">
                            <a title="Options" href="#"><i class="glyphicon glyphicon-cog"></i></a>
                            <a data-widgster="expand" title="Expand" href="#"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a>
                            <a data-widgster="collapse" title="Collapse" href="#"><i
                                    class="glyphicon glyphicon-chevron-down"></i></a>
                            <a data-widgster="close" title="Close" href="#"><i
                                    class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </header>
                    <div class="body">
                        <div class="button-group">
                            <button class="btn-primary btn" onclick="openModal('add')"><i
                                    class="glyphicon glyphicon-plus"></i>
                                Add Voucher</button>
                            <button class="btn-default btn" data-toggle="modal" data-target="#modalFormExcel"><i
                                    class="glyphicon glyphicon-upload"></i> Upload Voucher (Excel)</button>
                        </div>
                        <!-- modal import excel -->
                        <div class="modal fade" id="modalFormExcel" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Import Excel</h4>
                                    </div>
                                    <form id="submit" action="javascript:void(0);" enctype="multipart/form-data"
                                        method="POST">
                                        <div class="modal-body">
                                            <div class="well">
                                                <i class="fa-info-circle fa"></i> Information
                                                <p>
                                                    Please download template below before uploading<br>
                                                    <a href="http://localhost/googleplay/excel/kerjaan.xlsx"
                                                        class="btn btn-info">Download
                                                        Template</a>
                                                </p>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputExcel" class="control-label">Excel File</label>
                                                <input type="file" name="file" id="file" class="form-control">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i>
                                                Save</button>
                                            <button class="btn btn-danger" data-dismiss="modal" type="reset"><i
                                                    class="fa fa-times"></i>
                                                Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="well">

                            <form action="<?php echo site_url('Data/multi'); ?>" method="get">
                                <table class="table">
                                    <tr>
                                        <td>Denom <input type="text" name="refillId" placeholder="REFILL ID"
                                                value="<?php echo (isset($_GET['refillId'])) ? $_GET['refillId'] : ''; ?>"
                                                class="form-control">
                                        </td>

                                        <td>Tanggal Purchase<input type="date" name="tanggalPurchase"
                                                placeholder="Enter tanggalPurchase"
                                                value="<?php echo (isset($_GET['tanggalPurchase'])) ? $_GET['tanggalPurchase'] : ''; ?>"
                                                class="form-control">
                                        </td>
                                    </tr>
                                </table>
                                <button type="submit" class="btn btn-danger">Cari Data</button> <a
                                    href="<?php echo base_url('index.php/Data'); ?>" class="btn btn-default">Reset</a>
                            </form>
                        </div>

                        <div class="table-responsive well">
                            <table id="pindah" class="table table-hover dataTableCustom">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Serial Number</th>
                                        <th>Refill ID</th>
                                        <th>Upload Date</th>
                                        <th>Status</th>
                                        <th style="width:100px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tblBody">
                                    <?php
                                    if (count($data) > 0) {
                                        $no = 1;
                                        foreach ($data as $row) { ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $row->serialNumber; ?></td>
                                        <td><?php echo $row->refillId; ?></td>
                                        <td><?php echo $row->tanggalUpload; ?></td>
                                        <td><?php echo ubahStatus($row->status_voucher); ?></td>
                                        <td>
                                            <a href="" class="btn btn-default"><i
                                                    class="glyphicon glyphicon-pencil"></i></a>
                                            <a href="" class="btn btn-danger"><i
                                                    class="glyphicon glyphicon-trash"></i></a></td>
                                    </tr>
                                    <?php }
                                } else { ?>
                                    <tr>
                                        <td colspan="5" align="center">Tidak Ada Data.</td>
                                    </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col">
                                    <!--Tampilkan pagination-->
                                    <?php echo $pagination; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <footer class="content-footer">
            Games Dashboard - Made by <a href="https://shiblysolution.com" rel="nofollow noopener noreferrer"
                target="_blank">STS</a>
        </footer>
    </div>
    </div>



    <!-- common libraries. required for every page-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="<?php echo base_url('lib/bootstrap-sass/assets/javascripts/bootstrap.min.js'); ?>"></script>
    <script>
    $(document).ready(function() {
        // alert("oke gaes");
        $('#submit').submit(function(e) {
            e.preventDefault();
            // alert("hello")
            $.ajax({
                url: "<?php echo base_url('index.php/Data/import'); ?>",
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                dataType: "JSON",
                success: function(data) {
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        // title: result.msg,
                        html: '<div style="font-size:16px;">' + data.msg +
                            '</div>',
                        showConfirmButton: false,
                        timer: 2000
                    });
                    $('input[name=file]').val('');
                    $('#modalFormExcel').modal('hide');
                }
            });
        });


    });
    </script>

</body>

</html>